##
# Ansible playbooks
#
# @file
# @version 0.1
.PHONY: $(MAKECMDGOALS) all
.DEFAULT_GOAL := help

PROJECT_NAME=ansible-playbooks

##@ General

# The help target prints out all targets with their descriptions organized
# beneath their categories. The categories are represented by '##@' and the
# target descriptions by '##'. The awk commands is responsible for reading the
# entire set of makefiles included in this invocation, looking for lines of the
# file as xyz: ## something, and then pretty-format the target and help. Then,
# if there's a line with ##@ something, that gets pretty-printed as a category.
# More info on the usage of ANSI control characters for terminal formatting:
# https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_parameters
# More info on the awk command:
# http://linuxcommand.org/lc3_adv_awk.php

help: 						## Display this help.
	@printf -- "${FORMATTING_BEGIN_BLUE} ${PROJECT_NAME} ${FORMATTING_END}\n"
	@printf -- "\n"
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target> playbook=my_fancy_playbook.yaml\033[0m\n"} /^[a-zA-Z_0-9-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

playbook=workstation.yml

run: 	## Run main playbook
	ansible-playbook -i hosts $(playbook)

##@ Setup
deps:	## Install dependencies
	pip install -r requirements.txt

hosts:	## Lists all hosts
	ansible -i hosts all --list-hosts

##@ Nondirenv
alt-setup: ## python virtual environment
	python -m venv .venv
	VIRTUAL_ENV_DISABLE_PROMPT=true source .venv/bin/activate;\
	pip install --upgrade pip;\
	pip install -r requirements.txt

alt-run: ## run main playbook
	VIRTUAL_ENV_DISABLE_PROMPT=true source .venv/bin/activate;\
	env ANSIBLE_FORCE_COLOR=true ansible-playbook -i hosts workstation.yml
